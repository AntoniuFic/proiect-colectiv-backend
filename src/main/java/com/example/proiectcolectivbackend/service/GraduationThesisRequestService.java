package com.example.proiectcolectivbackend.service;

import com.example.proiectcolectivbackend.api.exceptions.GenericBadRequestException;
import com.example.proiectcolectivbackend.api.exceptions.ResourceNotFoundException;
import com.example.proiectcolectivbackend.api.model.GraduationThesisRequestDto;
import com.example.proiectcolectivbackend.api.model.RespondToRequest;
import com.example.proiectcolectivbackend.api.model.StudentDto;
import com.example.proiectcolectivbackend.api.model.SubscribeToProfessorBody;
import com.example.proiectcolectivbackend.datasource.model.*;
import com.example.proiectcolectivbackend.datasource.repository.GraduationThesisRequestRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.text.MessageFormat;
import java.util.Date;
import java.util.List;

import static com.example.proiectcolectivbackend.datasource.model.GraduationThesisRequestStatus.*;

@Slf4j
@Service
@RequiredArgsConstructor
public class GraduationThesisRequestService {
    private final GraduationThesisRequestRepository graduationThesisRequestRepository;
    private final ProfessorService professorService;
    private final StudentService studentService;
    private final GraduationThesisService graduationThesisService;

    private static final String UNIQUE_STUDENT_PROFESSOR_CONSTRAINT =
            "unique_student_professor_constraint";
    private static final String UNIQUE_STUDENT_PROFESSOR_MESSAGE =
            "Request already exists between this student and the requested professor";
    private static final String STATE_CHANGE_ERROR_MESSAGE = "Cannot transit from {0} to {1}";
    private static final String REQUEST_NOT_FOUND_ERROR_MESSAGE = "Graduation thesis request not found";

    public List<GraduationThesisRequestDto> findAllRequestsByUser(User user) {
        if (user instanceof Professor professor) {
            return toGraduationThesisRequestDtoList(findAllRequestsByProfessor(professor));
        } else {
            return toGraduationThesisRequestDtoList(findAllRequestsByStudent((Student) user));
        }
    }

    public GraduationThesisRequestDto sendRequestToProfessor(
            SubscribeToProfessorBody subscribeToProfessorBody,
            Student principal) {
        principal = studentService.getById(principal.getId());//remake hibernate session
        if (principal.hasActiveGraduationThesis()) {
            throw new GenericBadRequestException("Cannot send requests, you already have an active thesis");
        }
        if (principal.hasAcceptedOtherRequest()) {
            throw new GenericBadRequestException("Cannot send other requests if you accepted one");
        }

        int professorId = subscribeToProfessorBody.getProfessorId();
        var professor = professorService.getProfessorById(professorId);

        GraduationThesisRequest graduationThesisRequest = GraduationThesisRequest.builder()
                .requestSentAt(new Date())
                .requesterStudent(principal)
                .status(PENDING)
                .requestedProfessor(professor)
                .studentInitialMessage(subscribeToProfessorBody.getStudentInitialMessage())
                .build();
        try {
            return toGraduationThesisRequestDto(
                    graduationThesisRequestRepository.save(graduationThesisRequest));
        } catch (Exception e) {
            if (e.getMessage().contains(UNIQUE_STUDENT_PROFESSOR_CONSTRAINT)) {
                throw new GenericBadRequestException(UNIQUE_STUDENT_PROFESSOR_MESSAGE);
            }
            throw e;
        }
    }

    public GraduationThesisRequestDto respondToGraduationThesisRequest(RespondToRequest respondToRequest,
                                                                       User user,
                                                                       int graduationThesisRequestId) {
        var graduationThesisRequest = findGraduationThesisRequestById(graduationThesisRequestId);
        if (user instanceof Professor professor) {
            return handleProfessorResponse(professor, graduationThesisRequest, respondToRequest);
        } else {
            return handleStudentResponse((Student) user, graduationThesisRequest, respondToRequest);
        }
    }

    private GraduationThesisRequestDto handleProfessorResponse(Professor professor,
                                                               GraduationThesisRequest graduationThesisRequest,
                                                               RespondToRequest respondToRequest) {
        professor = professorService.getProfessorById(professor.getId());//initialize hibernate session
        log.info("Handling professorId: {} response to graduation thesis request id: {}",
                professor.getId(), graduationThesisRequest.getGraduationThesisRequestId());
        if (respondToRequest.getProfessorResponseMessage().isBlank()) {
            throw new GenericBadRequestException("The message cannot be blank");
        }
        if (respondToRequest.getProfessorResponseMessage().length() > 100) {
            throw new GenericBadRequestException("The message length should be smaller then 100");
        }
        if (graduationThesisRequest.getRequestedProfessor().getId() != professor.getId()) {
            throw new GenericBadRequestException("This graduation thesis not owned by this professor");
        }

        var nextStatus = respondToRequest.getNextStatus();
        validateNextStatusAsProfessor(graduationThesisRequest.getStatus(), nextStatus);
        if (!professor.canAcceptStudent() && nextStatus.equals(PROFESSOR_ACCEPTED)) {
            throw new GenericBadRequestException("This professor has no more places for students");
        }

        graduationThesisRequest.setStatus(nextStatus);
        graduationThesisRequest.setProfessorResponseMessage(respondToRequest.getProfessorResponseMessage());

        var savedGraduationThesisRequest = graduationThesisRequestRepository.save(graduationThesisRequest);
        return toGraduationThesisRequestDto(savedGraduationThesisRequest);

    }

    private GraduationThesisRequestDto handleStudentResponse(Student student,
                                                             GraduationThesisRequest graduationThesisRequest,
                                                             RespondToRequest respondToRequest) {
        log.info("Handling student id: {} response to graduation thesis request id: {}",
                student.getId(), graduationThesisRequest.getGraduationThesisRequestId());

        student = studentService.getById(student.getId());//we must remake the hibernate session
        if (graduationThesisRequest.getRequesterStudent().getId() != student.getId()) {
            throw new GenericBadRequestException("This graduation thesis was not sent by this student");
        }
        var nextStatus = respondToRequest.getNextStatus();
        validateNextStatusAsStudent(graduationThesisRequest.getStatus(), nextStatus);
        if (student.hasAcceptedOtherRequest()) {
            throw new GenericBadRequestException("You can accept only one professor");
        }

        graduationThesisRequest.setStatus(nextStatus);
        var savedGraduationThesisRequest = graduationThesisRequestRepository.save(graduationThesisRequest);
        if (nextStatus == STUDENT_ACCEPTED) {
            student.markNonAcceptedRequestsAsInvalid();
        }
        studentService.save(student);
        return toGraduationThesisRequestDto(savedGraduationThesisRequest);
    }

    private void validateNextStatusAsProfessor(GraduationThesisRequestStatus currentStatus,
                                               GraduationThesisRequestStatus nextStatus) {
        boolean transitionOk = currentStatus.equals(PENDING);
        if (!nextStatus.isInProfessorAcceptedNextStatus()) {
            transitionOk = false;
        }
        if (!transitionOk) {
            var message = MessageFormat.format(STATE_CHANGE_ERROR_MESSAGE,
                    currentStatus.name(), nextStatus.name());
            throw new GenericBadRequestException(message);
        }
    }

    private void validateNextStatusAsStudent(GraduationThesisRequestStatus currentStatus,
                                             GraduationThesisRequestStatus nextStatus) {
        boolean transitionOk = currentStatus.equals(PROFESSOR_ACCEPTED);
        if (!nextStatus.isInStudentAcceptedNextStatus()) {
            transitionOk = false;
        }
        if (!transitionOk) {
            var message = MessageFormat.format(STATE_CHANGE_ERROR_MESSAGE,
                    currentStatus.name(), nextStatus.name());
            throw new GenericBadRequestException(message);
        }
    }

    private GraduationThesisRequest findGraduationThesisRequestById(int id) {
        return graduationThesisRequestRepository.findById(id).orElseThrow(
                () -> new ResourceNotFoundException(REQUEST_NOT_FOUND_ERROR_MESSAGE));
    }

    private GraduationThesisRequestDto toGraduationThesisRequestDto(GraduationThesisRequest request) {
        return GraduationThesisRequestDto.builder()
                .thesisRequestId(request.getGraduationThesisRequestId())
                .professorId(request.getRequestedProfessor().getId())
                .studentId(request.getRequesterStudent().getId())
                .status(request.getStatus())
                .initialStudentMessage(request.getStudentInitialMessage())
                .professorResponseMessage(request.getProfessorResponseMessage())
                .requestSentAt(request.getRequestSentAt())
                .studentInfo(toStudentDto(request.getRequesterStudent()))
                .build();
    }

    private List<GraduationThesisRequest> findAllRequestsByProfessor(Professor professor) {
        return graduationThesisRequestRepository.findAllByRequestedProfessor(professor);
    }

    private List<GraduationThesisRequest> findAllRequestsByStudent(Student student) {
        return graduationThesisRequestRepository.findAllByRequesterStudent(student);
    }

    private List<GraduationThesisRequestDto> toGraduationThesisRequestDtoList(List<GraduationThesisRequest> requests) {
        return requests.stream().map(this::toGraduationThesisRequestDto).toList();
    }

    private StudentDto toStudentDto(Student student) {
        return StudentDto.builder()
                .studentId(student.getId())
                .facultyDepartment(student.getFacultyDepartment())
                .email(student.getEmail())
                .fullName(student.getFullName())
                .studyLanguage(student.getStudyLanguage())
                .studentGroup(student.getStudentGroup())
                .graduationThesis(graduationThesisService.toDto(student.getGraduationThesis()))
                .build();
    }
}
