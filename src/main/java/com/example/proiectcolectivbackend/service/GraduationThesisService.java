package com.example.proiectcolectivbackend.service;

import com.example.proiectcolectivbackend.api.exceptions.GenericBadRequestException;
import com.example.proiectcolectivbackend.api.exceptions.ResourceNotFoundException;
import com.example.proiectcolectivbackend.api.model.GraduationThesisBody;
import com.example.proiectcolectivbackend.api.model.GraduationThesisDto;
import com.example.proiectcolectivbackend.api.model.GraduationThesisTitleBody;
import com.example.proiectcolectivbackend.datasource.model.GraduationThesis;
import com.example.proiectcolectivbackend.datasource.model.Professor;
import com.example.proiectcolectivbackend.datasource.model.Student;
import com.example.proiectcolectivbackend.datasource.model.User;
import com.example.proiectcolectivbackend.datasource.repository.GraduationThesisRepository;
import com.example.proiectcolectivbackend.datasource.repository.StudentRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class GraduationThesisService {
    private final ProfessorService professorService;
    private final StudentRepository studentRepository;
    private final GraduationThesisRepository graduationThesisRepository;

    public GraduationThesisDto add(GraduationThesisBody graduationThesisBody, Student stud) {
        Professor professor = professorService.getProfessorById(graduationThesisBody.getProfessorCoordinatorId());
        stud = studentRepository.findById(stud.getId()).orElseThrow(() -> new ResourceNotFoundException("Not found"));
        if (!stud.markAcceptedRequestAsOutdated(professor)) {
            throw new GenericBadRequestException("Cannot add graduation thesis");
        }

        GraduationThesis graduationThesis = GraduationThesis.builder()
                .professorCoordinator(professor)
                .student(stud)
                .thesisTitle(graduationThesisBody.getThesisTitle())
                .build();
        studentRepository.save(stud);
        return toDto(graduationThesisRepository.save(graduationThesis));
    }

    public GraduationThesisDto toDto(GraduationThesis graduationThesis) {
        if (graduationThesis == null) {
            return null;
        }
        return GraduationThesisDto.builder()
                .id(graduationThesis.getGraduationThesisId())
                .professorCoordinatorId(graduationThesis.getProfessorCoordinator().getId())
                .thesisTitle(graduationThesis.getThesisTitle())
                .build();
    }

    public GraduationThesisDto changeTitle(int id, GraduationThesisTitleBody body, User principal) {
        int principalId = principal.getId();
        return graduationThesisRepository.findById(id).map(gt -> {
            if (principalId != gt.getProfessorCoordinator().getId() &&
                    principalId != gt.getStudent().getId()) {
                throw new GenericBadRequestException("Not allowed");
            }
            gt.setThesisTitle(body.getTitle());
            graduationThesisRepository.save(gt);
            return toDto(gt);
        }).orElseThrow(() -> new ResourceNotFoundException("Graduation thesis not found"));
    }
}
