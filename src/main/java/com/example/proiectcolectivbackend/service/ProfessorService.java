package com.example.proiectcolectivbackend.service;

import com.example.proiectcolectivbackend.api.exceptions.ResourceNotFoundException;
import com.example.proiectcolectivbackend.api.model.ProfessorDto;
import com.example.proiectcolectivbackend.datasource.model.Professor;
import com.example.proiectcolectivbackend.datasource.repository.ProfessorRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class ProfessorService {
    private final ProfessorRepository professorRepository;

    public List<ProfessorDto> getAllProfessors() {
        var professors = professorRepository.findAll();
        return professors.stream().map(this::toProfessorDto).toList();
    }

    public ProfessorDto getProfessorDtoById(int professorId) {
        return toProfessorDto(getProfessorById(professorId));
    }

    public Professor getProfessorById(int professorId) {
        var professor = professorRepository.findById(professorId);
        if (professor.isEmpty()) {
            throw new ResourceNotFoundException("Professor not found");
        }
        return professor.get();
    }

    public ProfessorDto increaseMaximumNumberOfStudents(Professor professor, int amount) {
        professor = this.getProfessorById(professor.getId());
        professor.increaseMaximumNumberOfStudents(amount);
        return toProfessorDto(professorRepository.save(professor));
    }

    private ProfessorDto toProfessorDto(Professor professor) {
        var numberOfEnrolledStudents = professor.getNumberOfEnrolledStudents();
        return ProfessorDto.builder()
                .professorId(professor.getId())
                .maximumNumberOfStudents(professor.getMaximumNumberOfStudents())
                .domainOfActivity(professor.getDomainOfActivity())
                .name(professor.getFullName())
                .numberOfEnrolledStudents(numberOfEnrolledStudents)
                .title(professor.getTitle())
                .department(professor.getDepartment())
                .supportedLanguages(professor.getSupportedLanguages())
                .build();
    }
}
