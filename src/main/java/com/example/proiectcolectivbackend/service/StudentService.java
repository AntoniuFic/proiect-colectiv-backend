package com.example.proiectcolectivbackend.service;

import com.example.proiectcolectivbackend.api.exceptions.ResourceNotFoundException;
import com.example.proiectcolectivbackend.api.model.StudentDto;
import com.example.proiectcolectivbackend.datasource.model.Student;
import com.example.proiectcolectivbackend.datasource.repository.GraduationThesisRepository;
import com.example.proiectcolectivbackend.datasource.repository.ProfessorRepository;
import com.example.proiectcolectivbackend.datasource.repository.StudentRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class StudentService {
    private final StudentRepository studentRepository;
    private final GraduationThesisRepository graduationThesisRepository;
    private final ProfessorRepository professorRepository;
    private final GraduationThesisService graduationThesisService;

    public List<StudentDto> getAllStudents() {
        return studentRepository.findAll().stream().map(this::toStudentDto).toList();
    }

    public StudentDto getStudentById(int id) {
        var student = studentRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("Student not found"));
        return toStudentDto(student);
    }

    public Student getById(int id) {
        return studentRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("Student not found"));
    }

    public List<StudentDto> getStudentsByProfessor(int professorId) {
        if (!professorRepository.existsById(professorId)) {
            throw new ResourceNotFoundException("Professor not found");
        }

        return graduationThesisRepository
                .findGraduationThesisByProfessorCoordinatorId(professorId)
                .stream().map(graduationThesis -> toStudentDto(graduationThesis.getStudent()))
                .toList();
    }

    private StudentDto toStudentDto(Student student) {
        return StudentDto.builder()
                .studentId(student.getId())
                .facultyDepartment(student.getFacultyDepartment())
                .email(student.getEmail())
                .fullName(student.getFullName())
                .studyLanguage(student.getStudyLanguage())
                .studentGroup(student.getStudentGroup())
                .graduationThesis(graduationThesisService.toDto(student.getGraduationThesis()))
                .build();
    }

    public void save(Student student) {
        studentRepository.save(student);
    }
}
