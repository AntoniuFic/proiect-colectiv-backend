package com.example.proiectcolectivbackend.service;

import com.example.proiectcolectivbackend.datasource.model.*;
import com.example.proiectcolectivbackend.datasource.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.CommandLineRunner;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Service
@RequiredArgsConstructor
public class AddUser implements CommandLineRunner {
    private final PasswordEncoder passwordEncoder;
    private final UserRepository userRepository;

    @Override
    public void run(String... args) throws Exception {
        String email = "Vasile@gmail.com";
        String pass = "mamaliga";
        String fullName = "Vasile Popa";

        Professor prof1 = Professor.builder()
                .email(email)
                .password(passwordEncoder.encode(pass))
                .fullName(fullName)
                .title("Drd")
                .domainOfActivity("Info")
                .supportedLanguages(Set.of(Language.RO, Language.EN))
                .department(Department.INFORMATICS)
                .maximumNumberOfStudents(10)
                .build();

        Student stud1 = Student.builder()
                .email("ionStud@gmail.com")
                .password(passwordEncoder.encode("varza"))
                .fullName("Ionel Biciclentiu")
                .facultyDepartment(Department.INFORMATICS)
                .studentGroup(203)
                .studyLanguage(Language.EN)
                .build();

        GraduationThesis graduationThesis = GraduationThesis.builder()
                .thesisTitle("Development of AI")
                .professorCoordinator(prof1)
                .student(stud1)
                .build();
        prof1.setGraduationTheses(List.of(graduationThesis));

        Student stud2 = Student.builder()
                .email("ionStud2@gmail.com")
                .password(passwordEncoder.encode("varza2"))
                .fullName("Gigel corabierul")
                .facultyDepartment(Department.MATHEMATICS)
                .studentGroup(233)
                .studyLanguage(Language.RO)
                .build();

        Student stud3 = Student.builder()
                .email("ionStud3@gmail.com")
                .password(passwordEncoder.encode("varza3"))
                .fullName("Gigel corabierul")
                .facultyDepartment(Department.MATHEMATICS_INFORMATICS)
                .studentGroup(233)
                .studyLanguage(Language.HUN)
                .build();

        userRepository.saveAll(List.of(prof1, stud1, stud2, stud3));
        addProfessorsFromCsv();
    }

    public void addProfessorsFromCsv() {
        try {

            InputStream resource = getClass().getResourceAsStream("/profesori-editat.csv");
            BufferedReader reader = new BufferedReader(new InputStreamReader(resource));

            String line;
            var professors = new ArrayList<Professor>();
            while ((line = reader.readLine()) != null) {
                String[] professorData = line.split(",");

                String fullName = professorData[0].trim();
                String email = professorData[1].trim();
                String title = professorData[2].trim();
                String domainOfActivity = professorData[3].trim();
                int maximumNumberOfStudents = Integer.parseInt(professorData[4].trim());
                String department = professorData[5].trim();
                String languages = professorData[6].trim();
                //String profilePictureUrl = professorData[7].trim();

                Professor professor = Professor.builder()
                        .email(email)
                        .password(passwordEncoder.encode("mamaliga"))
                        .fullName(fullName)
                        .title(title)
                        .domainOfActivity(domainOfActivity)
                        .supportedLanguages(parseLanguages(languages))
                        .department(Department.valueOf(department.toUpperCase()))
                        .maximumNumberOfStudents(maximumNumberOfStudents)
                        .build();
                professors.add(professor);
            }
            userRepository.saveAll(professors);

            reader.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private Set<Language> parseLanguages(String languages) {
        Set<Language> languageSet = new HashSet<>();
        String[] languageArray = languages.split("_");
        for (String language : languageArray) {
            languageSet.add(Language.valueOf(language));
        }
        return languageSet;
    }
}
