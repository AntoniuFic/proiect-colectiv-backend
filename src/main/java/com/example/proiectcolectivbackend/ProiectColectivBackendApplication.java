package com.example.proiectcolectivbackend;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ProiectColectivBackendApplication {

    public static void main(String[] args) {
        SpringApplication.run(ProiectColectivBackendApplication.class, args);
    }

}
