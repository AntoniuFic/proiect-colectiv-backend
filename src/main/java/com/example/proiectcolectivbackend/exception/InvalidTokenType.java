package com.example.proiectcolectivbackend.exception;

public class InvalidTokenType extends RuntimeException{
    public InvalidTokenType() {
    }

    public InvalidTokenType(String message) {
        super(message);
    }
}
