package com.example.proiectcolectivbackend.api.model;

import com.example.proiectcolectivbackend.datasource.model.GraduationThesisRequestStatus;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.Size;
import lombok.Data;

@Data
public class RespondToRequest {
    private GraduationThesisRequestStatus nextStatus;
    @NotBlank(message = "Professor response message can't be blank")
    @Size(min = 1, max = 100)
    private String professorResponseMessage;
}
