package com.example.proiectcolectivbackend.api.model;

import jakarta.validation.constraints.NotBlank;
import lombok.Data;

@Data
public class GraduationThesisTitleBody {
    @NotBlank
    private String title;
}
