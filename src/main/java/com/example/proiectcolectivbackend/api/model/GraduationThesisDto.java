package com.example.proiectcolectivbackend.api.model;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class GraduationThesisDto {
    private int id;
    private int professorCoordinatorId;
    private String thesisTitle;
}
