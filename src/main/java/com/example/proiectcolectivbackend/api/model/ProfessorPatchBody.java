package com.example.proiectcolectivbackend.api.model;

import jakarta.validation.constraints.Positive;
import lombok.Data;

@Data
public class ProfessorPatchBody {
    @Positive
    Integer increaseMaxNumberStudentsBy;
}
