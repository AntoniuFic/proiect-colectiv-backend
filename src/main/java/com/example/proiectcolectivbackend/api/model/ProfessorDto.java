package com.example.proiectcolectivbackend.api.model;

import com.example.proiectcolectivbackend.datasource.model.Department;
import com.example.proiectcolectivbackend.datasource.model.Language;
import lombok.Builder;
import lombok.Getter;

import java.util.Set;

@Builder
@Getter
public class ProfessorDto{
    private int professorId;
    private String name;
    private String domainOfActivity;
    private int maximumNumberOfStudents;
    private int numberOfEnrolledStudents;
    private String title;
    private Department department;
    private Set<Language> supportedLanguages;
}
