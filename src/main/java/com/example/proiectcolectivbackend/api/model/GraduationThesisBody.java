package com.example.proiectcolectivbackend.api.model;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.Positive;
import lombok.Data;

@Data
public class GraduationThesisBody {
    @Positive
    private int professorCoordinatorId;
    @NotBlank
    private String thesisTitle;
}

