package com.example.proiectcolectivbackend.api.model;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.Positive;
import jakarta.validation.constraints.Size;
import lombok.Data;

@Data
public class SubscribeToProfessorBody {
    @Positive(message = "Id's must be positive")
    private int professorId;
    @NotBlank(message = "Student initial message shouldn't be blank")
    @Size(min = 1, max = 100)
    private String studentInitialMessage;
}
