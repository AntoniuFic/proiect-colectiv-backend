package com.example.proiectcolectivbackend.api.model;

import com.example.proiectcolectivbackend.datasource.model.Department;
import com.example.proiectcolectivbackend.datasource.model.Language;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Builder;
import lombok.Getter;

@Builder
@Getter
@JsonInclude(JsonInclude.Include.NON_NULL)
public class StudentDto {
    private int studentId;
    private String email;
    private String fullName;
    private Language studyLanguage;
    private Department facultyDepartment;
    private int studentGroup;
    private GraduationThesisDto graduationThesis;
}
