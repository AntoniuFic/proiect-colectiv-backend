package com.example.proiectcolectivbackend.api.model;

import com.example.proiectcolectivbackend.datasource.model.GraduationThesisRequestStatus;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Builder;
import lombok.Getter;

import java.util.Date;

@Getter
@Builder
@JsonInclude(JsonInclude.Include.NON_NULL)
public class GraduationThesisRequestDto {
    private int thesisRequestId;
    private int studentId;
    private int professorId;
    private String initialStudentMessage;
    private String professorResponseMessage;
    private Date requestSentAt;
    GraduationThesisRequestStatus status;
    private StudentDto studentInfo;
}
