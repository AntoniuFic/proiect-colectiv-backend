package com.example.proiectcolectivbackend.controller.authentication;

import com.example.proiectcolectivbackend.datasource.model.Roles;
import lombok.*;

@Getter
@Builder
@AllArgsConstructor
public class AuthenticationResponseBody {
    private String authenticationToken;
    private String refreshToken;
    private int userId;
    private Roles role;

    public AuthenticationResponseBody() {
    }
}
