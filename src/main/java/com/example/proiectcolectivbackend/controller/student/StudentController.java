package com.example.proiectcolectivbackend.controller.student;

import com.example.proiectcolectivbackend.api.model.StudentDto;
import com.example.proiectcolectivbackend.service.StudentService;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import jakarta.validation.constraints.Positive;
import lombok.RequiredArgsConstructor;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api/students")
@RequiredArgsConstructor
@Validated
public class StudentController {
    private final StudentService studentService;

    @SecurityRequirement(name = "bearerAuthentication")
    @GetMapping
    public List<StudentDto> getAllStudents() {
        return studentService.getAllStudents();
    }

    @SecurityRequirement(name = "bearerAuthentication")
    @GetMapping("/{id}")
    public StudentDto getAllStudents(@Positive @PathVariable int id) {
        return studentService.getStudentById(id);
    }
}
