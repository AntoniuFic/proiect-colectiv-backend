package com.example.proiectcolectivbackend.controller.graduation_thesis;

import com.example.proiectcolectivbackend.api.model.GraduationThesisRequestDto;
import com.example.proiectcolectivbackend.api.model.RespondToRequest;
import com.example.proiectcolectivbackend.api.model.SubscribeToProfessorBody;
import com.example.proiectcolectivbackend.datasource.model.Student;
import com.example.proiectcolectivbackend.datasource.model.User;
import com.example.proiectcolectivbackend.service.GraduationThesisRequestService;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import jakarta.validation.Valid;
import jakarta.validation.constraints.Positive;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/graduation-thesis-requests")
@RequiredArgsConstructor
@Validated
public class GraduationThesisRequestController {
    private final GraduationThesisRequestService graduationThesisRequestService;

    @SecurityRequirement(name = "bearerAuthentication")
    @PostMapping
    public ResponseEntity<GraduationThesisRequestDto> sendRequestToProfessor(
            @AuthenticationPrincipal Student principal,
            @Valid @RequestBody SubscribeToProfessorBody subscribeToProfessorBody) {
        var requestDto = graduationThesisRequestService
                .sendRequestToProfessor(subscribeToProfessorBody, principal);
        return ResponseEntity.ok(requestDto);
    }

    @PatchMapping("/{graduationThesisRequestId}")
    @SecurityRequirement(name = "bearerAuthentication")
    public ResponseEntity<GraduationThesisRequestDto> respondToRequest(
            @AuthenticationPrincipal User principal,
            @RequestBody RespondToRequest respondToRequest,
            @Positive @PathVariable(name = "graduationThesisRequestId") int graduationThesisRequestId) {
        var requestDto = graduationThesisRequestService
                .respondToGraduationThesisRequest(respondToRequest, principal, graduationThesisRequestId);
        return ResponseEntity.ok(requestDto);
    }

    @GetMapping
    @SecurityRequirement(name = "bearerAuthentication")
    public ResponseEntity<List<GraduationThesisRequestDto>> getGraduationThesisRequests(
            @AuthenticationPrincipal User user) {
        return ResponseEntity.ok(graduationThesisRequestService.findAllRequestsByUser(user));
    }
}
