package com.example.proiectcolectivbackend.controller.graduation_thesis;

import com.example.proiectcolectivbackend.api.model.GraduationThesisBody;
import com.example.proiectcolectivbackend.api.model.GraduationThesisDto;
import com.example.proiectcolectivbackend.api.model.GraduationThesisTitleBody;
import com.example.proiectcolectivbackend.datasource.model.Student;
import com.example.proiectcolectivbackend.datasource.model.User;
import com.example.proiectcolectivbackend.service.GraduationThesisService;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/graduation-thesis")
@RequiredArgsConstructor
@Validated
public class GraduationThesisController {
    private final GraduationThesisService graduationThesisService;

    @SecurityRequirement(name = "bearerAuthentication")
    @PostMapping
    public ResponseEntity<GraduationThesisDto> add(
            @AuthenticationPrincipal Student principal,
            @Valid @RequestBody GraduationThesisBody graduationThesisBody) {
        var graduationThesisDto = graduationThesisService.add(graduationThesisBody, principal);
        return ResponseEntity.ok(graduationThesisDto);
    }

    @SecurityRequirement(name = "bearerAuthentication")
    @PatchMapping("/{id}")
    public ResponseEntity<GraduationThesisDto> changeTitle(
            @AuthenticationPrincipal User principal,
            @Valid @RequestBody GraduationThesisTitleBody body,
            @PathVariable("id") int id) {
        return ResponseEntity.ok(graduationThesisService.changeTitle(id, body, principal));
    }
}
