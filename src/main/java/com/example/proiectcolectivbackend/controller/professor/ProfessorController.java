package com.example.proiectcolectivbackend.controller.professor;

import com.example.proiectcolectivbackend.api.model.ProfessorDto;
import com.example.proiectcolectivbackend.api.model.ProfessorPatchBody;
import com.example.proiectcolectivbackend.api.model.StudentDto;
import com.example.proiectcolectivbackend.datasource.model.Professor;
import com.example.proiectcolectivbackend.service.ProfessorService;
import com.example.proiectcolectivbackend.service.StudentService;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import jakarta.validation.Valid;
import jakarta.validation.constraints.Positive;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/professors")
@RequiredArgsConstructor
@Validated
public class ProfessorController {
    private final ProfessorService professorService;
    private final StudentService studentService;

    @SecurityRequirement(name = "bearerAuthentication")
    @GetMapping
    public List<ProfessorDto> getAllProfessors() {
        return professorService.getAllProfessors();
    }

    @SecurityRequirement(name = "bearerAuthentication")
    @GetMapping("/{id}/students")
    public List<StudentDto> getStudentsByProfessor(@Positive @PathVariable int id) {
        return studentService.getStudentsByProfessor(id);
    }

    @SecurityRequirement(name = "bearerAuthentication")
    @GetMapping("/{id}")
    public ResponseEntity<ProfessorDto> getProfessorById(@Positive @PathVariable int id) {
        var professor = professorService.getProfessorDtoById(id);
        return ResponseEntity.ok(professor);
    }

    @SecurityRequirement(name = "bearerAuthentication")
    @PatchMapping("/me")
    public ResponseEntity<ProfessorDto> increaseMaximumNumberOfStudents(
            @AuthenticationPrincipal Professor professor,
            @Valid @RequestBody ProfessorPatchBody professorUpdated) {
        return ResponseEntity.ok(professorService.increaseMaximumNumberOfStudents(professor,
                professorUpdated.getIncreaseMaxNumberStudentsBy()));
    }

}
