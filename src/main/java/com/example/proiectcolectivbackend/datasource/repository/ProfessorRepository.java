package com.example.proiectcolectivbackend.datasource.repository;

import com.example.proiectcolectivbackend.datasource.model.Professor;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ProfessorRepository extends JpaRepository<Professor, Integer> {
    Professor findProfessorByEmail(String email);
}
