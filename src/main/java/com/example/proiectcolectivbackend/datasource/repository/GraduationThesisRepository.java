package com.example.proiectcolectivbackend.datasource.repository;

import com.example.proiectcolectivbackend.datasource.model.GraduationThesis;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface GraduationThesisRepository extends JpaRepository<GraduationThesis, Integer> {
    List<GraduationThesis> findGraduationThesisByProfessorCoordinatorId(int id);
}
