package com.example.proiectcolectivbackend.datasource.repository;

import com.example.proiectcolectivbackend.datasource.model.GraduationThesisRequest;
import com.example.proiectcolectivbackend.datasource.model.Professor;
import com.example.proiectcolectivbackend.datasource.model.Student;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface GraduationThesisRequestRepository extends JpaRepository<GraduationThesisRequest, Integer> {
    List<GraduationThesisRequest> findAllByRequesterStudent(Student student);

    List<GraduationThesisRequest> findAllByRequestedProfessor(Professor professor);
}
