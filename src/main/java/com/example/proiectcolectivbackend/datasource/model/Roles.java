package com.example.proiectcolectivbackend.datasource.model;

import java.text.MessageFormat;

public enum Roles {
    STUDENT, PROFESSOR;

    public static String getRoleName(Roles roles) {
        return MessageFormat.format("ROLE_{0}", roles.name());
    }
}
