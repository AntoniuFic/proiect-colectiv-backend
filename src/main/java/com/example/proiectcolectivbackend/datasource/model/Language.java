package com.example.proiectcolectivbackend.datasource.model;

public enum Language {
    RO,EN,GER,HUN
}
