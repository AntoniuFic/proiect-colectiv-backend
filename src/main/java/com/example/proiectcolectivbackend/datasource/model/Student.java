package com.example.proiectcolectivbackend.datasource.model;

import jakarta.persistence.*;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.RequiredArgsConstructor;
import lombok.experimental.SuperBuilder;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

import java.util.Collection;
import java.util.List;

import static com.example.proiectcolectivbackend.datasource.model.GraduationThesisRequestStatus.*;

@EqualsAndHashCode(callSuper = true)
@Data
@Entity
@SuperBuilder
@RequiredArgsConstructor
public class Student extends User {
    @Basic(optional = false)
    private int studentGroup;
    @Basic(optional = false)
    private Department facultyDepartment;
    @Basic(optional = false)
    private Language studyLanguage;
    @OneToOne(fetch = FetchType.LAZY, mappedBy = "student", cascade = CascadeType.ALL)
    private GraduationThesis graduationThesis;
    @OneToMany(fetch = FetchType.LAZY,
            cascade = CascadeType.ALL, mappedBy = "requesterStudent",
            orphanRemoval = true)
    private List<GraduationThesisRequest> graduationThesisRequests;

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return List.of(new SimpleGrantedAuthority(Roles.getRoleName(Roles.STUDENT)));
    }

    public boolean hasAcceptedOtherRequest() {
        return graduationThesisRequests.stream()
                .anyMatch(gtr -> gtr.getStatus().equals(STUDENT_ACCEPTED));
    }

    /**
     * @param professor professor requested
     * @return true if student can mark accepted request as outdated,
     * false otherwise
     */
    public boolean markAcceptedRequestAsOutdated(Professor professor) {
        var request = graduationThesisRequests.stream()
                .filter(gtr -> gtr.getStatus().equals(STUDENT_ACCEPTED) &&
                        gtr.getRequestedProfessor().equals(professor)).findAny();
        if (request.isEmpty()) {
            return false;
        }
        request.get().setStatus(OUTDATED);
        return true;
    }


    public void markNonAcceptedRequestsAsInvalid() {
        graduationThesisRequests.stream()
                .filter(gtr -> {
                    var status = gtr.getStatus();
                    return status != STUDENT_ACCEPTED && status != STUDENT_DECLINED;
                })
                .forEach(gtr -> {
                    var replaceWithStatus = gtr.getStatus() == PROFESSOR_ACCEPTED ? STUDENT_DECLINED : OUTDATED;
                    gtr.setStatus(replaceWithStatus);
                });
    }

    public boolean hasActiveGraduationThesis() {
        return graduationThesis != null;
    }
}
