package com.example.proiectcolectivbackend.datasource.model;

import jakarta.persistence.*;
import lombok.*;

import java.io.Serializable;

@Entity
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class GraduationThesis implements Serializable {

    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    private int graduationThesisId;
    @ManyToOne(fetch = FetchType.LAZY)
    private Professor professorCoordinator;
    @OneToOne(fetch = FetchType.LAZY)
    private Student student;
    private String thesisTitle;
}