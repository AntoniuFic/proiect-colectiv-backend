package com.example.proiectcolectivbackend.datasource.model;

public enum Department {
    INFORMATICS, MATHEMATICS, MATHEMATICS_INFORMATICS
}
