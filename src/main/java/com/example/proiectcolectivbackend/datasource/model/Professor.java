package com.example.proiectcolectivbackend.datasource.model;

import jakarta.persistence.*;

import lombok.Data;
import lombok.EqualsAndHashCode;

import lombok.RequiredArgsConstructor;
import lombok.experimental.SuperBuilder;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

import java.util.Collection;
import java.util.List;
import java.util.Set;

import static com.example.proiectcolectivbackend.datasource.model.GraduationThesisRequestStatus.*;

@EqualsAndHashCode(callSuper = true)
@Entity
@Data
@RequiredArgsConstructor
@SuperBuilder
public class Professor extends User {
    @Basic(optional = false)
    private String domainOfActivity;
    @Basic(optional = false)
    private String title;
    @Basic(optional = false)
    private int maximumNumberOfStudents;
    @Basic(optional = false)
    Department department;
    private Set<Language> supportedLanguages;
    @OneToMany(fetch = FetchType.LAZY,
            cascade = CascadeType.ALL, mappedBy = "professorCoordinator",
            orphanRemoval = true)
    private List<GraduationThesis> graduationTheses;
    @OneToMany(fetch = FetchType.LAZY,
            cascade = CascadeType.ALL, mappedBy = "requestedProfessor",
            orphanRemoval = true)
    private List<GraduationThesisRequest> graduationThesisRequests;

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return List.of(new SimpleGrantedAuthority(Roles.getRoleName(Roles.PROFESSOR)));
    }

    public void increaseMaximumNumberOfStudents(int amount) {
        this.maximumNumberOfStudents += amount;
        graduationThesisRequests.removeIf(value -> value.getStatus().equals(PROFESSOR_DECLINED));
    }

    public int getNumberOfEnrolledStudents() {
        int numberOfGraduationThesis = graduationTheses.size();
        int numberOfAcceptedGraduationThesisRequests = graduationThesisRequests.stream()
                .filter(gtr -> gtr.getStatus().equals(PROFESSOR_ACCEPTED) || gtr.getStatus().equals(STUDENT_ACCEPTED))
                .toList().size();
        return numberOfAcceptedGraduationThesisRequests + numberOfGraduationThesis;
    }

    public boolean canAcceptStudent() {
        return maximumNumberOfStudents - getNumberOfEnrolledStudents() > 0;
    }
}
