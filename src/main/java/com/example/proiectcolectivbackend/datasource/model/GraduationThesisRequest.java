package com.example.proiectcolectivbackend.datasource.model;

import jakarta.persistence.*;
import lombok.*;

import java.io.Serializable;
import java.util.Date;

@Entity
@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "graduation_thesis_rquest",
        uniqueConstraints = @UniqueConstraint(columnNames = {"requester_student_id", "requested_professor_id"},
                name = "unique_student_professor_constraint"))
public class GraduationThesisRequest implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int graduationThesisRequestId;
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    private Student requesterStudent;
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    private Professor requestedProfessor;
    @Column(nullable = false)
    private String studentInitialMessage;
    private String professorResponseMessage;
    @Column(nullable = false)
    private GraduationThesisRequestStatus status;
    private Date requestSentAt;
}
