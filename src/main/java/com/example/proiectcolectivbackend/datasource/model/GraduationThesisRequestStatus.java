package com.example.proiectcolectivbackend.datasource.model;

public enum GraduationThesisRequestStatus {
    PENDING, PROFESSOR_ACCEPTED, PROFESSOR_DECLINED, STUDENT_ACCEPTED, STUDENT_DECLINED, OUTDATED;

    public boolean isInProfessorAcceptedNextStatus() {
        return this.equals(PROFESSOR_ACCEPTED) || this.equals(PROFESSOR_DECLINED);
    }

    public boolean isInStudentAcceptedNextStatus() {
        return this.equals(STUDENT_ACCEPTED) || this.equals(STUDENT_DECLINED);
    }
}
